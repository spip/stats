# Changelog

## 3.2.0 - 2024-11-25

### Added

- Installable en tant que package Composer

### Changed

- Utiliser `#LAYOUT_PRIVE` à la place de `#LARGEUR_ECRAN`
- spip/spip#4657 Mettre tout ce qui est administration des statistiques sur une page à part accessible via le menu maintenance
- Compatible SPIP 5.0.0-dev

### Fixed

- Url dans le phpdoc de genie_popularites_dist
